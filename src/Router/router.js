import React from 'react';
import {HashRouter, Route, Switch ,Redirect} from 'react-router-dom';
import Login from "../Views/Login";
import Index from "../Views/Index"
import Register from "../Views/register"
import quil from "../Components/quil";
const BasicRoute = () =>(
    <HashRouter>
        <Switch>
            <Route exact path="/index" component={Index}/>
            <Route exact path="/" component={Login}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/test" component = {quil}/>
        </Switch>
    </HashRouter>
)


export default BasicRoute;
