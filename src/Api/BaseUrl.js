//必须要写成localhost，否则取不到ctx.session;
// const BasePath = "http://106.52.23.236:7003/";
const BasePath = "http://localhost:7001/";
const serverMainPath = {
    getUserData:BasePath + 'admin/getUserInfo/',
    userInfo: BasePath + 'admin/folders',
    addNotebook: BasePath + 'admin/addNotebook/',
    notebook: BasePath + 'admin/notebook',
    addFolder: BasePath + 'admin/addFolder/',
    uploadFile: BasePath + 'admin/uploadFile',
    uploadimg: BasePath + 'admin/uploadImg',
    getUploadFilesList:BasePath+'admin/getUploadFilesList/',
    beforeUpload:BasePath+ 'admin/beforeUpload/',
    updateNickName:BasePath+'admin/updateNickName/',
    updatePassWord:BasePath+'admin/updatePassWord',
    updateFolderName:BasePath+'admin/updateFolderName/',
    deleteFolder:BasePath+'admin/deleteFolder/',
    downloadFiles:BasePath+'admin/downloadFiles/',
    deleteFile:BasePath+'admin/deleteFile/',
    getDeleteFolders:BasePath+'admin/getDeleteFolders',
    saveContent:BasePath+'admin/saveContent/',
    searchNotebook:BasePath + 'admin/searchNotebook/'
}
const serverLoginPath = {
    //验证码
    captcha: BasePath + 'login/captcha',
    //登录
    login: BasePath + 'login/login',
    //注册
    register: BasePath + 'login/register',
    //注册信息
    registerInfo: BasePath + 'login/registerInfo',
    //登出
    logOut: BasePath + 'login/logOut',
}

export {serverMainPath, serverLoginPath};
