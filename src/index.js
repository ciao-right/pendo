import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import Router from '../src/Router/router'
import axios from "./Api/api";
import { Provider } from 'react-redux'
import store from '../src/store/index.js';
React.Component.prototype.axios = axios;

ReactDOM.render(
    <Provider store={store}>
        <Router/>
    </Provider>,
  document.getElementById('root')
);


