import React, {Component} from 'react';
// import Main from "../Containers/Main";
import Index from "./Index";
import {connect} from 'react-redux';
class App extends Component {

    render() {
        const maindiv = {
            height:'100%'
        };
        return (

           <div style={maindiv}>
               <Index></Index>
           </div>
        );
    }
}

export default App;
