import React from "react";
import "../Style/register.scss";
import {withRouter} from "react-router-dom";
import LoginHeader from "../Components/LoginHeader";
import done from "../Images/done.png";
import {Checkbox, Steps, Divider, Input, Button, Form, message, Select} from "antd";
import axios from "../Api/api";
import {serverLoginPath} from "../Api/BaseUrl";

const {Step} = Steps;
const {Option} = Select;
const Register = (props) => {
    const prefixSelector = (
        <Form.Item name="prefix" noStyle initialValue="+86">
            <Select style={{width: 70}}>
                <Option value="86">+86</Option>
                <Option value="87">+87</Option>
            </Select>
        </Form.Item>
    );
    const layout = {
        labelCol: {span: 6},
        wrapperCol: {span: 16},
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0,
            },
            sm: {
                span: 16,
                offset: 8,
            },
        },
    };
    const onFinish = values => {
        setRegisterUsername(values.username);
        axios.post(serverLoginPath.register, values).then(res => {
            if (res.code === 200) {
                setStepStatus("success");
                next();
            } else if (res.code === 406) {
                message.error("用户名重复", 2);
                setStepStatus("error");
            }
        })
    };
    const onFinishForstep2 = values => {
        values.username = registerUsername;
        console.log(values)
        axios.post(serverLoginPath.registerInfo, values).then(res => {
            if (res.code === 200) {
                setStepStatus("success");
                next();
            } else {
                message.error("未知错误", 2, onclose);
                setStepStatus("error");
            }
        })
    };
    const onFinishFailed = errorInfo => {
        message.error('Failed:', errorInfo);
        setStepStatus("error");
    };
    const stepContent1 = <div className="register_form">

        <Form
            name="step1"
            initialValues={{remember: true}}
            labelAlign="right"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            {...layout}
        >
            <Form.Item
                name="username"
                label="用户名"
                rules={[
                    {
                        required: true,
                        message: '请输入您的用户名',
                    },
                ]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                name="password"
                label="密码"
                rules={[
                    {
                        required: true,
                        message: '请输入您的密码',
                    },
                ]}
                hasFeedback
            >
                <Input.Password/>
            </Form.Item>

            <Form.Item
                name="confirm"
                label="确认密码"
                dependencies={['password']}
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: '请再次输入您的密码',
                    },
                    ({getFieldValue}) => ({
                        validator(rule, value) {
                            if (!value || getFieldValue('password') === value) {
                                return Promise.resolve();
                            }

                            return Promise.reject('两次密码输入不一致');
                        },
                    }),
                ]}
            >
                <Input.Password/>
            </Form.Item>
            <Form.Item
                name="agreement"
                valuePropName="checked"
                rules={[
                    {
                        validator: (_, value) =>
                            value ? Promise.resolve() : Promise.reject('需要同意协议'),
                    },
                ]}
                {...tailFormItemLayout}
            >
                <Checkbox>
                    用户勾选即代表同意 <a href="https://reg.163.com/agreement_wap.shtml?v=20171127">我的协议</a>
                </Checkbox>
            </Form.Item>


            <div className="steps-action">
                <Button type="primary" htmlType="submit">
                    下一步
                </Button>
            </div>


        </Form>
    </div>
    const stepContent2 = <div className="register_form">

        <Form
            name="step2"

            labelAlign="right"
            onFinish={onFinishForstep2}
            onFinishFailed={onFinishFailed}
            {...layout}
        >
            <Form.Item
                name="email"
                label="E-mail"
                rules={[
                    {
                        type: 'email',
                        message: '您输入的不是正确的邮箱格式',
                    },
                    {
                        required: true,
                        message: '请输入您的邮箱',
                    },
                ]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                name="nickname"
                label="昵称"
                rules={[
                    {
                        required: true,
                        message: '请输入您的昵称',
                    },
                ]}

            >
                <Input/>
            </Form.Item>

            <Form.Item
                name="phoneNum"
                label="手机号"
                rules={[
                    {
                        required: true,
                        message: '请输入您的手机号',
                    }
                ]}
            >
                <Input addonBefore={prefixSelector} style={{width: '100%'}}/>
            </Form.Item>


            <div className="steps-action">
                <Button type="primary" htmlType="submit">
                    下一步
                </Button>
                <Button style={{margin: '0 8px'}} onClick={() => prev()}>
                    上一步
                </Button>
            </div>


        </Form>
    </div>
    const stepContent3 = <div className="register_form">
        <div className="doneImg">
            <img src={done} alt=""/>
        </div>
        <div className="doneRegister">
            <p>恭喜你，完成注册！</p>
        </div>
        <div className="steps-action">
            <Button type="primary" onClick={() => {
                message.success('完成注册!')
                props.history.push({pathname: '/'});
            }}>
                去登陆
            </Button>
            <Button style={{margin: '0 8px'}} onClick={() => prev()}>
                上一步
            </Button>
        </div>


    </div>
    const steps = [
        {
            title: '输入邮箱密码',
            content: stepContent1,
        },
        {
            title: '输入基本信息',
            content: stepContent2,
        },
        {
            title: '完成注册',
            content: stepContent3,
        },
    ];


    const [current, setCurrent] = React.useState(0);
    const [stepStatus, setStepStatus] = React.useState("");
    const [registerUsername, setRegisterUsername] = React.useState("");

    const next = () => {
        setCurrent(current + 1);
    };

    const prev = () => {
        setStepStatus("success");
        setCurrent(current - 1);
    };
    return (
        <div className="register_container">
            <LoginHeader/>
            <div className="register_body">
                <div className="register_header">
                    <p>开始注册你的Pendo吧！</p>
                </div>
                <Divider/>
                <Steps current={current} status={stepStatus}>
                    {steps.map(item => (
                        <Step key={item.title} title={item.title}/>
                    ))}
                </Steps>
                {/*内容*/}
                <div className="steps-content">{steps[current].content}</div>
                {/*按钮*/}
                <div className="steps-action">
                </div>


            </div>
        </div>
    )
}
export default withRouter(Register);
