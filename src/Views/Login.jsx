import React, {Component} from 'react';
import LoginHeader from "../Components/LoginHeader";
import NormalLoginForm from '../Components/LoginFrom';
import style from "../Style/Login.module.scss"


class Login extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

render() {
    const loginFormstyle = {
        margin: "0 auto",
        background: "#fff",
        height: "100%",
        paddingTop: "80px",

    }
    const constyle = {
        height: "calc(100% - 50px)"

    }

        return (
            <div style={constyle}>
                           <LoginHeader/>
                           <div style={loginFormstyle} className="site-card-border-less-wrapper">
                               <NormalLoginForm className={style.form}/>
                           </div>
                       </div>
        );
    }
}

export default Login;
