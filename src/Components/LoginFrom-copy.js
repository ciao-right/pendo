import React from "react";
import {Form, Input, Button, Checkbox} from 'antd';
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import {withRouter} from 'react-router-dom'
import axios from "../Api/api";

class NormalLoginForm extends React.Component {
    constructor(props) {
        super(props);
    }
    state={
        userInfo:{
            userName:'',
            passWord:''
        }}


    render() {
        // const obj2 = Object.assign({userName: '',passWord:''},this.state.data)
        const onFinish = values => {
            console.log('Received values of form: ', values);
        };

        function handleClickLogin() {
            let that = this;
            axios.post(global.webUrl + '/index', that.state.userInfo).then((res) => {
                if (res.message === 'success')
                    this.props.history.push('/');
            })

        }

        return (
            <Form
                name="normal_login"
                className="login-form"
                initialValues={{remember: true}}
                onFinish={onFinish}
            >
                <Form.Item

                    name="username"
                    rules={[{required: true, message: '请输入您的用户名!'}]}
                >
                    <Input  value={this.state.userInfo.userName}  onChange={(e)=>{this.setState({userName: e.target.value} )
                        console.log(this.state.userInfo.userName)}} prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="用户名"/>
                </Form.Item
                >
                <Form.Item
                    name="password"
                    rules={[{required: true, message: '请输入您的密码!'}]}
                >
                    <Input
                        value={this.state.userInfo.passWord}  onChange={(e)=>{this.setState({passWord : e.target.value})}}
                        prefix={<LockOutlined className="site-form-item-icon"/>}
                        type="password"
                        placeholder="密码"
                        style={{}}
                    />
                </Form.Item>
                <Form.Item style={{textAlign: "center"}}>
                    <Form.Item name="remember" valuePropName="checked" noStyle>
                        <Checkbox>记住密码</Checkbox>
                    </Form.Item>

                    <a className="login-form-forgot" href="#">
                        忘记密码
                    </a>
                </Form.Item>

                <Form.Item style={{textAlign: "center", display: 'flex',}}>
                    <Button type="primary" className="login-form-button" style={{marginRight: "30px"}}
                            onClick={handleClickLogin}>
                        登录
                    </Button>
                    <a className="login-form-forgot">现在注册</a>
                </Form.Item>
            </Form>
        );

    }
}

export default withRouter(NormalLoginForm);
