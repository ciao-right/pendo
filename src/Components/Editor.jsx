import React, {Component, useEffect, useRef, useState} from "react";
import E from 'wangeditor'
import 'highlight.js/styles/monokai-sublime.css'
import hljs from 'highlight.js'

//使用hooks
const Editor = (props) => {
    let editor = null;
    let [editorContent, setEditorContent] = useState('');
    editor = new E("#editorElem")
    useEffect(() => {
        // const elem = this.refs.editorElem;
        // const editor = new E(editorElem)
        // 使用 onchange 函数监听内容的变化，并实时更新到 state 中
        editor.customConfig.onchange = html => {
            props.toGetContent(html);
            setEditorContent(
               html
            )


            // editor.txt.html(html)
        }

        editor.highlight = hljs;

        editor.customConfig.zIndex = 100;

        editor.create();
        editor.config.languageType = [
            'Bash',
            'C',
            'C#',
            'C++',
            'CSS',
            'Java',
            'JavaScript',
            'JSON',
            'TypeScript',
            'Plain text',
            'Html',
            'XML',
            'SQL',
            'Go',
            'Kotlin',
            'Lua',
            'Markdown',
            'PHP',
            'Python',
            'Shell Session',
            'Ruby',
        ]
        let eds = document.getElementsByClassName('w-e-text-container')
        eds[0].style = eds[0].style.cssText + 'height: 100%';
        eds[0].style = eds[0].style.cssText + 'width: 100%';

        editor.txt.html(props.bookContent)
    }, [props.bookContent]);

    const clickHandle = () => {

        console.log(props.bookContent);

    }
    return (
        <div style={{width: '98%', height: '90%', marginRight: '20px'}}>
            {/* 将生成编辑器 */}
            <div id="editorElem" style={{textAlign: 'left', marginTop: '20px', width: '100%', height: '100%'}}>
            </div>

            {/*<button style={{position: "absolute", zIndex: 100}} onClick={clickHandle}>获取内容</button>*/}
        </div>
    );
};

// class Editor extends Component {
//     constructor(props, context) {
//         super(props, context);
//         this.state = {
//             readyEditor: props.bookContent,
//
//         }
//
//     }
//     componentWillReceiveProps(nextProps) {
//         console.log(nextProps)
//
//         this.setState({readyEditor: nextProps.bookContent});
//         // this.editor.txt.html(this.state.readyEditor)
//     }
//     render() {
//         return (
//             <div style={{width: '98%', height: '90%', marginRight: '20px'}}>
//                 {/* 将生成编辑器 */}
//                 <div ref="editorElem" style={{textAlign: 'left', marginTop: '20px', width: '100%', height: '100%'}}>
//                 </div>
//
//                 <button style={{position: "absolute", zIndex: 100}} onClick={this.clickHandle.bind(this)}>获取内容</button>
//             </div>
//         );
//     }
//
//
//
//
//     componentDidMount() {
//
//         const elem = this.refs.editorElem
//         const editor = new E(elem)
//         // editor.config.height = 500;
//         editor.customConfig.zIndex = 100;
//         // editor.txt.html({this.state.editorContent})
//         // 使用 onchange 函数监听内容的变化，并实时更新到 state 中
//
//         console.log(this.state.readyEditor)
//         editor.create();
//
//         editor.txt.html(this.state.readyEditor);
//         // editor.customConfig.onchange = html => {
//         //     // console.log(this.state.editorContent);
//         //     // editor.txt.html(this.state.editorContent);
//         //     // this.setState({
//         //     //     // editorContent: editor.txt.text()
//         //     //     editorContent: editor.txt.html()
//         //     // })
//         //
//         // }
//         // editor.customConfig.debug = location.href.indexOf('wangeditor_debug_mode=1') > 0
//
//         // console.log();
//         let eds = document.getElementsByClassName('w-e-text-container')
//         eds[0].style = eds[0].style.cssText + 'height: 100%';
//         eds[0].style = eds[0].style.cssText + 'width: 100%';
//
//     }
//     //
//
//
//
//     clickHandle() {
//         // this.setState({
//         //     editorContent : "背背背背起了行囊"
//         // })
//         console.log(this.props.bookContent);
//
//     }
// }
//
//
export default Editor;


// shouldComponentUpdate 这个方法是当props或者state状态改变时调用
// componentWillUpdate：在组件接手到新的props或者state，但是render还没有调用时调用
// componentDidUpdate：在组件完成更新后立即调用。在初始化时不被调用
// render：页面渲染函数
// componentWillUnmount：组件从DOM中移除时立即调用。(当页面离开时调用)
