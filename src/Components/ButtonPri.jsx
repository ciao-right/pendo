import React from "react";
import '../Style/uploadFile.scss'
import {Button, Dropdown, Menu, Modal, message, Upload,} from 'antd';
import {DownOutlined, PlusCircleTwoTone, InboxOutlined} from '@ant-design/icons';
import {serverMainPath} from "../Api/BaseUrl";
import axios from "../Api/api";

const {Dragger} = Upload;


// const customRequest = (option)=>{
//     const formData = new FormData();
//     const fileUrl = serverMainPath.uploadFile;
//     formData.append('files[]',option.file);
//
// }


class ButtonPri extends React.Component {
    state = {
        visible: false,
        valueInput: '',
        bjvisible: false,
        bjvalueInput: '',
        fileVisible: false,
        fileValueInput: ''
    };

    constructor(props) {
        super(props);
this.getUploadFilesList.bind(this);
    };



    showModal = () => {
        this.setState({
            visible: true,

        });
    };
    showModalForFile = () => {
        this.setState({
            fileVisible: true,

        })
    }
    fileHandleOk = () => {

    }
    // bjshowModal = () => {
    //     this.setState({
    //
    //         bjvisible: true
    //     });
    // };
    handleOk = () => {
        this.props.handlewjjName(this.state.valueInput);
        this.setState({
            visible: false,
            valueInput: '',
        });
    };

    bjhandleOk = () => {
        this.props.handlebjName(this.state.bjvalueInput);
        this.setState({
            bjvisible: false,
            bjvalueInput: '',
        });

    };

    handleCancel = e => {
        // console.log(e);

        this.setState({
            visible: false,
            valueInput: '',
            bjvisible: false,
            bjvalueInput: '',
            fileVisible: false,
            fileValueInput: '',
            uploadUrl:''
        });
    };

    getUploadFilesList() {
        axios(
          serverMainPath.getUploadFilesList + this.state.userInfo.username
        ).then((res) => {
          if (res.data.length < 0) return;
    
          // console.log(res.data);
          this.state.fileList = res.data;
        });
      }


    render() {

        const props = {
            name: 'file',
            multiple: true,
            action: this.state.uploadUrl,
            data:{username:  JSON.parse(sessionStorage.getItem('userInfo')).username},
            beforeUpload:(file,filelist)=>{
               console.log(file);
               const pattern = /image/;
               if(pattern.test(file.type)){
                   this.setState({
                       uploadUrl:serverMainPath.uploadimg
                   })
               }else {
                   this.setState({
                       uploadUrl:serverMainPath.uploadFile
                   })
               }
            },
            onChange(info) {
                const {status} = info.file;
                if (status !== 'uploading') {
                }
                if (status === 'done') {
                    
                    message.success(`${info.file.name} 文件上传成功`);
                    

                } else if (status === 'error') {
                    message.error(`${info.file.name} 文件上传失败`);
                }
            },
        };

        
   // console.log(info)
                    // let beforeUpLoadFile  =  JSON.parse(sessionStorage.getItem('userInfo'));
                    // console.log(beforeUpLoadFile)
                    // axios(serverMainPath.beforeUpload+beforeUpLoadFile.username).then((res)=>{
                    //     if (res.code === 401){
                    //         message.error('发生了未知错误',2);
                    //     }
                    // })


        const btnstyle = {
            height: 40 + 'px',
            width: 100 + '%',
            backgroundColor: '#F5F5F5',
            color: '#393939',
            border: '#fff'
        };
        const btntext = {
            marginRight: 20 + "px",
        }
        const menu = (
            <Menu>
                {/*<Menu.Item key="0">*/}
                {/*    <span onClick={this.bjshowModal.bind(this)}>新建笔记</span>*/}
                {/*</Menu.Item>*/}
                <Menu.Item key="0">
                    <span onClick={this.showModal.bind(this)}>新建文件夹</span>
                </Menu.Item>

                <Menu.Item key="2">
                    <span onClick={this.showModalForFile.bind(this)}>上传文件</span>
                </Menu.Item>
            </Menu>
        );

        return (
            <div>
                <Button style={btnstyle} type="primary" icon={<PlusCircleTwoTone/>} size="large">

                    <span style={btntext}>
                        <Dropdown overlay={menu} trigger={['click']} placement={"bottomCenter"}>
                        <div>
                            {this.props.btntext} <DownOutlined/>
                        </div>
                    </Dropdown>
                    </span>
                </Button>
                {/*新建文件夹 模拟框*/}
                <Modal
                    title="新建文件夹"
                    visible={this.state.visible}
                    onOk={this.handleOk.bind(this)}
                    onCancel={this.handleCancel.bind(this)}
                    cancelText="取消"
                    okText="确认"
                    bodyStyle={{zIndex: 100000}}
                    destroyOnClose="true"

                >
                    <div style={{display: "flex", justifyContent: "spaceAround"}}>
                        <span style={{marginRight: "10px"}}>文件夹名称:</span>
                        <input type="text" value={this.state.valueInput} onChange={(e) => {
                            this.setState({valueInput: e.target.value})
                        }} style={{width: "80%"}}/>
                    </div>
                </Modal>

                {/*新建笔记 模拟框*/}
                <Modal
                    title="新建笔记"
                    visible={this.state.bjvisible}
                    onOk={this.bjhandleOk.bind(this)}
                    onCancel={this.handleCancel.bind(this)}
                    cancelText="取消"
                    okText="确认"
                    bodyStyle={{zIndex: 100000}}
                    destroyOnClose="true"


                >
                    <div style={{display: "flex", justifyContent: "spaceAround"}}>
                        <span style={{marginRight: "10px"}}>笔记名称:</span>
                        <input type="text" value={this.state.bjvalueInput} onChange={(e) => {
                            this.setState({bjvalueInput: e.target.value})
                        }} style={{width: "80%"}}/>
                    </div>
                </Modal>

                <Modal
                    title="文件上传"
                    visible={this.state.fileVisible}
                    onOk={this.fileHandleOk.bind(this)}
                    onCancel={this.handleCancel.bind(this)}
                    cancelText=""
                    okText=""
                    footer={null}
                    bodyStyle={{zIndex: 100000,height:450+"px"}}
                    destroyOnClose="true"

                >
                    <div style={{display: "flex", justifyContent: "spaceAround"}}>
                        <Dragger {...props} style={{width: 100 + "%",height:70+"%"}} listType='picture-card'>
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined/>
                            </p>
                            <p className="ant-upload-text">请拖拽或点击此区域来上传文件</p>
                            <p className="ant-upload-hint" style={{width: 100 + "%"}}>
                                仅支持上传图片，表格，txt，word，markdown格式文档以及压缩文件，支持单次或批量上传。严禁上传其他文件
                            </p>
                        </Dragger>
                    </div>
                </Modal>


            </div>
        );
    }
}

export default ButtonPri;


