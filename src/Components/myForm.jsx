import React, {Component} from 'react';
import {Button, Spin, Input, Form, message} from "antd";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import axios from "../Api/api";
import {withRouter} from "react-router-dom";
import {serverLoginPath} from "../Api/BaseUrl"

class MyForm extends Component {
    state = {
        userInfo: {},
        loadFlag: false,
    }

    constructor(props) {
        super(props);
        this.loadingShow = this.loadingShow.bind(this);
    }

    componentDidMount() {
        // this.freshen();
    }


    loadingShow() {

        if (this.state.loadFlag === true) {
            return (
                <div style={{
                    width: 100 + "%",
                    height: 100 + "%",
                    backgroundColor: 'rgba(0,0,0,0.2)',
                    position: "absolute",
                    zIndex: 100,
                    top: 1 + 'px',
                    left: -1 + "px"
                }}>
                    <Spin
                        style={{position: "absolute", top: 50 + "%", left: 50 + "%", transform: 'translate(-50%,-50%)'}}
                        size="large"/>
                </div>
            )

        }
    }

    onFinish = values => {
        this.setState({
            userInfo: values
        });
        this.setState({
            loadFlag: true
        })
        axios.post(serverLoginPath.login, this.state.userInfo).then((res) => {

            // console.log(res.data,"[]")
            if (res.code === 200) {
                this.setState({
                    loadFlag: false
                });
                message.success('登录成功', 2, onclose);
                sessionStorage.setItem('userInfo', JSON.stringify(res.data));
                this.props.history.push({pathname: '/index'});

            } else if (res.code === 403) {
                this.setState({
                    loadFlag: false
                });
                message.error('用户名或密码错误', 2, onclose)
            } else if (res.code === 402) {
                this.setState({
                    loadFlag: false
                });
                message.error('验证码错误', 2, onclose)
            }

        })


    };

    //注册
    register() {
        // console.log("!1");
        this.props.history.push({pathname: '/register'});
    }

    //刷星验证码图片
    // freshen() {
    //     fetch('http://localhost:7001/captcha', {
    //         method: "GET",
    //         mode: 'cors',
    //     }).then(response => response.json()).then(res => {
    //         // console.log(res, "ppp");
    //         // sessionStorage.setItem('session_id_', res.data.uniqid)
    //         this.setState({
    //             captcha:' http://localhost:7001/captcha'
    //         })
    //     })
    // }

    render() {
        let captcha;
        if (this.props.placeholderName === '手机号') {
            captcha = null;
        } else {
            captcha = <Form.Item
                style={{width: 60 + "%", position: "relative"}}

            >
                <Form.Item name='code'
                           rules={[{required: true, message: '请输入验证码!'}]}
                >
                    <Input
                        type="text"
                        placeholder='验证码'
                    />
                </Form.Item>

                <div
                    style={{
                        position: "absolute",
                        zIndex: 100,
                        left: 210 + 'px',
                        top: 0 + "px",
                        width: 70 + "%"
                    }}>
                    <img src={serverLoginPath.captcha} style={{width: 100 + "%"}} alt="" title='点击刷新'/>
                </div>

            </Form.Item>
        }
        return (
            <div>
                {this.loadingShow()}
                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{remember: true}}
                    onFinish={this.onFinish.bind(this)}
                >
                    <Form.Item
                        name="username"
                        rules={[{required: true, message: '请输入用户名!'}]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon"/>}
                               placeholder={this.props.placeholderName}/>
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[{required: true, message: '请输入密码!'}]}
                    >
                        <Input
                            prefix={<LockOutlined className="site-form-item-icon"/>}
                            type="password"
                            placeholder={this.props.placeholderSecond}
                        />
                    </Form.Item>
                    {captcha}
                    {/*<Form.Item style={{textAlign: "left"}}>*/}
                    {/*    <Form.Item name="remember" valuePropName="checked" noStyle>*/}
                    {/*        <Checkbox>记住密码</Checkbox>*/}
                    {/*    </Form.Item>*/}

                    {/*    <a className="login-form-forgot" href="">*/}
                    {/*        忘记密码*/}
                    {/*    </a>*/}
                    {/*</Form.Item>*/}

                    <Form.Item style={{textAlign: "center"}}>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            登录
                        </Button>
                        {/*<a href="">注册</a>*/}
                        <Button type="primary" onClick={this.register.bind(this)}>
                            注册
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

export default withRouter(MyForm);
