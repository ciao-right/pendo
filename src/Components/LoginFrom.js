import React from "react";
import { Card} from 'antd';
import {QqOutlined, WechatOutlined, WeiboOutlined} from '@ant-design/icons';
import {withRouter} from 'react-router-dom'
import MyForm from './myForm'
// import axios from "../Api/api";
import '../Style/LoginForm.scss'

class NormalLoginForm extends React.Component {
    state = {
        loginTypeIndex: 0,
        loginType: ["账号密码登录", "手机验证码登录"],
    }

    constructor(props) {
        super(props);
        //    git push
    }

    // onFinish = values => {
    //     console.log('Received values of form: ', values);
    // };

    render() {
        const contentStyle = {
            height: '160px',
            color: '#fff',
            lineHeight: '160px',
            textAlign: 'center',
            background: '#364d79',
        };
        const cardBox = {
            display: 'flex',
            justifyContent: 'space-around',
            boxShadow: '10px 0px 20px #000',

        }
        const leftBox = {
            padding: '10px',
            paddingRight: '20px',
            height: '100%',
            width: '50%',
            borderRight: '#ccc 1px solid'
        }
        const rightBox = {
            width: '50%',
            height: '100%',
        }
        let myFormact;
        if (this.state.loginTypeIndex === 0) {
            myFormact = <MyForm placeholderName="账号" placeholderSecond="密码"/>
        } else {
            myFormact = <MyForm placeholderName="手机号" placeholderSecond="验证码"/>

        };
        return (
            <div>
                <Card style={cardBox} style={{
                    width: 800,
                    height: 500,
                    margin: '0 auto',
                    boxShadow: '0 1px 4px rgba(0,0,0,0.3),0 0 40px rgba(0,0,0,0.1) '
                }}
                      bodyStyle={{width: '100%', height: '100%', display: 'flex'}}>
                    <div style={leftBox}>
                        <div className="loginTitle">
                            <div className="boxs">
                                {
                                    this.state.loginType.map((item, index) => {
                                        return (
                                            <div className={this.state.loginTypeIndex === index ? "boxAct" : "box"}
                                                 key={index} onClick={() => {
                                                this.setState({loginTypeIndex: index})
                                            }}>
                                                <span>{item}</span>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>

                        {myFormact}

                    </div>
                    <div style={rightBox}>
                        <div className="box_container">
                            <div className="tips">
                                <p>其他账号登录：</p>
                            </div>
                            <div className="loginWays">
                                <div className="items">
                                    <div className="item">
                                        <div className="item_container">
                                            <QqOutlined style={{position:"relative",bottom:-7+"px"}} />
                                            <span>使用QQ账号登录</span>
                                        </div>

                                    </div>
                                    <div className="item">
                                        <div className="item_container">
                                            <WechatOutlined style={{position:"relative",bottom:-7+"px", color:"#6FC82B"}} />
                                            <span>使用微信账号登录</span>
                                        </div>

                                    </div>
                                    <div className="item">
                                        <div className="item_container">
                                            <WeiboOutlined  style={{position:"relative",bottom:-7+"px",color:"#EF1922"}} />
                                            <span>使用新浪微博账号登录</span>
                                        </div>

                                    </div>
                                    {/*<div className="item">*/}
                                    {/*    <div className="item_container">*/}
                                    {/*        <QqOutlined style={{position:"relative",bottom:-7+"px"}} />*/}
                                    {/*        <span>使用网易企业邮箱登录</span>*/}
                                    {/*    </div>*/}

                                    {/*</div>*/}
                                </div>
                            </div>
                        </div>
                    </div>
                </Card>
            </div>
        )


    }
}

export default withRouter(NormalLoginForm);
