import React,{Component} from "react";
import { Menu, Dropdown } from 'antd';

const menu = (
  <Menu>
    <Menu.Item key="1">1st menu item</Menu.Item>
    <Menu.Item key="2">2nd menu item</Menu.Item>
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

class RightClick extends Component {
    render() {
        return (
            <Dropdown overlay={menu} trigger={['contextMenu']}>

            </Dropdown>
        );
    }
}

export default RightClick;
