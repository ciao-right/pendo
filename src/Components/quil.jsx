import React, {useState} from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import {message} from "antd";

const MyComponent = ()=>{
    const [value, setValue] = useState('');
    // function imageHandler(action) {
    //     const input = document.createElement('input');
    //     input.setAttribute('type', 'file');
    //     input.setAttribute('accept', 'image/*');
    //     input.click();
    //     input.onchange = () => {
    //         const file = input.files[0];
    //         const fd = new FormData();
    //         fd.append('file', file);
    //         const hide = message.loading('上传中...', 0);
    //         upload(action, fd).then(res => {
    //             if (res) {
    //                 const range = this.quill.getSelection();
    //                 this.quill.insertEmbed(range.index, 'image', res.url);
    //                 hide();
    //             }
    //         });
    //     };
    // }
    const modules = {
        toolbar: {
            container: [
                [{ header: [1, 2,3,4,5 ,false] }],
                ['bold', 'italic', 'underline', 'strike', 'blockquote'],
                [{ list: 'ordered' }, { list: 'bullet' }],
                ['link', 'image'],
                ['clean'],
            ],
                handlers: {
                // image() {
                //     imageHandler.call(this, props.action);
                // }
            }
        }
    }
    const formats = [
        'header',
        'bold',
        'italic',
        'underline',
        'strike',
        'blockquote',
        'list',
        'bullet',
        'indent',
        'link',
        'image',
    ];
    return (
        <ReactQuill theme="snow" value={value} onChange={setValue}  modules={modules}
                    formats={formats} />
    );
}
export default MyComponent;