import React, {Component} from "react";
import E from 'wangeditor'
import store from "../store/index.js";
import {connect} from "react-redux";
//使用hooks
// const Editor = () => {
//     let [editorContent,setEditorContent] = useState('');
//     const editorElem = React.useRef(null);
//     useEffect(()=>{
//         // const elem = this.refs.editorElem;
//         const editor = new E(editorElem)
//         // 使用 onchange 函数监听内容的变化，并实时更新到 state 中
//         editor.customConfig.onchange = html => {
//             setEditorContent((html)=>{
//                 editorContent = html
//             })
//         }
//         editor.create()
//     })
//     return (
//         <div style={{height:100+"%",width:100+"%"}}>
//             <div ref={editorElem} style={{textAlign: 'left',height:100+"%",width:100+"%"}}>
//             </div>
//         </div>
//     );
// };

class Editor extends Component {
    constructor(props, context) {
        super(props, context);
        // this.state = {
        //     readyEditor: '',
        //     editorContent: '背背背背起了行囊'
        // }
        this.state = store.getState();
    }

    render() {
        return (
            <div style={{width: '98%', height: '90%', marginRight: '20px'}}>
                {/* 将生成编辑器 */}
                <div ref="editorElem" style={{textAlign: 'left', marginTop: '20px', width: '100%', height: '100%'}}>
                </div>

                <button style={{position: "absolute", zIndex: 100}} onClick={this.clickHandle.bind(this)}>获取内容</button>
            </div>
        );
    }



    componentDidMount() {
        const elem = this.refs.editorElem
        const editor = new E(elem)
        // editor.config.height = 500;
        editor.customConfig.zIndex = 100;
        // editor.txt.html({this.state.editorContent})
        // 使用 onchange 函数监听内容的变化，并实时更新到 state 中
        editor.customConfig.onchange = html => {
            // this.props.editorContent = editor.txt.html();
            // this.setState({
            //     // editorContent: editor.txt.text()
            //     editorContent: editor.txt.html()
            // })

        }
        // editor.customConfig.debug = location.href.indexOf('wangeditor_debug_mode=1') > 0
        editor.create();
        editor.txt.html(this.state.editorContent);
        let eds = document.getElementsByClassName('w-e-text-container')
        eds[0].style = eds[0].style.cssText + 'height: 100%';
        eds[0].style = eds[0].style.cssText + 'width: 100%'

    }

    clickHandle() {
        // this.setState({
        //     editorContent : "背背背背起了行囊"
        // })
        console.log(this.state.editorContent);
    }
}
const stateToProps = (state) =>{
    return {
        editorContent : state.editorContent
    }
}

export default connect(stateToProps,null)(Editor);
