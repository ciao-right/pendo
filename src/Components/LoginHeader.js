import React, {Component} from 'react';
import logo from '../Images/Logo.jpg';
import style from '../Style/Header.module.scss';
import {Avatar} from 'antd';
import { UserOutlined} from '@ant-design/icons';
import {withRouter} from "react-router-dom";


class Header extends Component {


    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <div className={style.header}>
                    <div className={style.logo}>
                        <img src={logo} alt=""/>
                        <span>Pendo</span>
                    </div>

                    <div className={style.users}>
                        <Avatar shape="square" size='large'
                                icon={<UserOutlined/>}
                                style={{marginRight: 20 + 'px'}}/>
                    </div>
                </div>

            </div>
        );
    }
}

export default withRouter(Header);
