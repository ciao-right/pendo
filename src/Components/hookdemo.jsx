import React,{useState,useEffect} from "react";
const hook = (props)=>{
    const [Count,setCount] = useState(0);
    return(
        <div>
            <button onClick={()=>{setCount(Count +1)}}></button>
            <p>{count}</p>
        </div>
    )
}
export default hook;


//useEffect
//用法：
// function Welcome(props) {
//     useEffect(() => {
//         document.title = `Hello, ${props.name}`;
//     }, [props.name]);
//     return <h1>Hello, {props.name}</h1>;
// }

//第一个参数为函数，第二个参数 为 依赖项是一个数组，当依赖项中的数据发生变换时，执行useEffect;
// 如果第二个参数是一个空数组，就表明副效应参数没有任何依赖项。因此，副效应函数这时只会在组件加载进入 DOM 后执行一次，后面组件重新渲染，就不会再次执行。
//只要是副效应，都可以使用useEffect()引入。它的常见用途有下面几种。
//
// 获取数据（data fetching）
// 事件监听或订阅（setting up a subscription）
// 改变 DOM（changing the DOM）
// 输出日志（logging）
//import React, { useState, useEffect } from 'react';
// import axios from 'axios';
//
// function App() {
//   const [data, setData] = useState({ hits: [] });
//
//   useEffect(() => {
//     const fetchData = async () => {
//       const result = await axios(
//         'https://hn.algolia.com/api/v1/search?query=redux',
//       );
//
//       setData(result.data);
//     };
//
//     fetchData();
//   }, []);
//
//   return (
//     <ul>
//       {data.hits.map(item => (
//         <li key={item.objectID}>
//           <a href={item.url}>{item.title}</a>
//         </li>
//       ))}
//     </ul>
//   );
// }
//
// export default App;
